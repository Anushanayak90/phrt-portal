/* eslint-disable no-debugger, no-console */
import React from "react";
import { SignInWrapper } from "./styled";

const HomePage = () => {
  /* const signin = () => {
    window.userManager
      .signinRedirect()
      .then((data: any) => {
        console.log("signinRedirect ok..................", data);
      })
      .catch((err: any) => {
        console.log("signinRedirect error:", err);
      });
  }; */
  return (
    <SignInWrapper>
      <div className="row no-gutters l-block">
        <div className="col-lg-6 p-0 l-block-bg" role="presentation" />
      </div>
    </SignInWrapper>
  );
};

export default HomePage;
