import styled from "styled-components";

const AdminFooterWrapper = styled.footer`
  background-color: #6d6e71;
  color: #fff;
  font-size: 16px;
  padding: 26px 24px;
`;
export default AdminFooterWrapper;
